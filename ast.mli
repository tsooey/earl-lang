type ident = string
and dec =
    TyDec of { tyId : ident; t : ty; }
  | VarDec of { id : ident; e : expr; }
  | TyVarDec of { id : ident; tyId : ident; e : expr; }
  | FunDec of { id : ident; fl : fieldDec list; e : expr; }
  | TyFunDec of { id : ident; fl : fieldDec list; tyId : ident; e : expr; }
and ty =
    TyId of { tyId : ident; }
  | ArrTy of { tyId : ident; }
  | RecTy of { fl : fieldDec list; }
and fieldDec = FieldDec of { id : ident; tyId : ident; }
and lVal =
    Id of { id : ident; }
  | Arr of { lval : lVal; e : expr; }
  | FieldExpr of { lval : lVal; id : ident; }
and expr =
    LVal of { lval : lVal; }
  | NilExpr
  | IntExpr of { num : int; }
  | SeqExpr of { el : expr list; }
  | NegExpr of { e : expr; }
  | InfixExpr of { e1 : expr; op : infixOp; e2 : expr; }
  | WhileExpr of { e1 : expr; e2 : expr; }
  | IfThenElseExpr of { e1 : expr; e2 : expr; e3 : expr; }
  | IfThenExpr of { e1 : expr; e2 : expr; }
  | LetExpr of { dl : dec list; el : expr list; }
  | ForExpr of { id : ident; init : expr; finish : expr; e : expr; }
and infixOp =
    Add
  | Sub
  | Mul
  | Div
  | Eq
  | Neq
  | Lt
  | Gt
  | Geq
  | Leq
  | And
  | Or
  | Asgn
val print_ast : expr -> unit
