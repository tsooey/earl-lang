{ 
  open Lexing
  open Parser
  open Ast

  exception SyntaxError of string 

  let next_line lexbuf = 
    let pos = lexbuf.lex_curr_p in 
    lexbuf.lex_curr_p <- 
      { pos with  pos_bol = lexbuf.lex_curr_pos; 
                  pos_lnum = pos.pos_lnum + 1
      }  
}

let digit = ['0'-'9']
let frac = '.' digit*
let exp = ['e' 'E'] ['-' '+']? digit+
let float = digit* frac? exp?
let white = [' ' '\t']+
let newline = '\r' | '\n' | "\r\n"
let id  = ['a'-'z']['a'-'z' 'A'-'Z' '0'-'9' '_']* 

rule token = parse 
      white               { token lexbuf }
    | newline             { next_line lexbuf; token lexbuf }
   
    (* Ints. *) 
    | ['0'-'9']+ as n     { INT(int_of_string n) } 
    
    (* Keywords. *)
    | "array"             { ARRAY } 
    | "if"                { IF }
    | "then"              { THEN }
    | "else"              { ELSE }
    | "while"             { WHILE }
    | "do"                { DO }
    | "let"               { LET } 
    | "in"                { IN }
    | "end"               { END }
    | "of"                { OF }
    | "break"             { BREAK} 
    | "nil"               { NIL }
    | "function"          { FUNCTION } 
    | "var"               { VAR } 
    | "type"              { TYPE }

    (* Identifiers. *)
    | id as i             { ID i }
   
    (* Punctuation. *) 
    | ','                 { COMMA }
    | ':'                 { COLON }
    | ';'                 { SEMICOLON } 
    | "("                 { LPAREN }
    | ")"                 { RPAREN }
    | "["                 { LBRACK }
    | "]"                 { RBRACK }
    | "{"                 { LBRACE }
    | "}"                 { RBRACE }
    | '.'                 { DOT }

    (* Operators. *)
    | '+'                 { PLUS }
    | '-'                 { MINUS }
    | '*'                 { TIMES }
    | '/'                 { DIVIDE }
    | '='                 { EQ }
    | "<>"                { NEQ } 
    | "<"                 { LT } 
    | ">"                 { GT }
    | ">="                { GEQ }
    | "<="                { LEQ }
    | "&"                 { AND }
    | "|"                 { OR }
    | ":="                { ASSIGN }
   
    (* Error routine. *) 
    | _ { raise (SyntaxError ("Unexpected char: " ^ Lexing.lexeme lexbuf)) }
    | eof                 { EOF }




