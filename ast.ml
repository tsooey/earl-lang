(* AST types *)
type ident = string 
and dec = 
    TyDec     of {tyId:ident; t:ty}
  | VarDec    of {id:ident; e:expr} 
  | TyVarDec  of {id:ident; tyId:ident; e:expr} 
  | FunDec    of {id:ident; fl:fieldDec list; e:expr}
  | TyFunDec  of {id:ident; fl:fieldDec list; tyId:ident; e:expr}
and ty = 
    TyId      of {tyId:ident}
  | ArrTy     of {tyId:ident}
  | RecTy     of {fl:fieldDec list}
and fieldDec = 
    FieldDec  of {id:ident; tyId:ident}
and lVal = 
    Id        of {id:ident} 
  | Arr       of {lval:lVal; e:expr}
  | FieldExpr of {lval:lVal; id:ident}
and expr = 
  | LVal      of {lval:lVal}
  | NilExpr
  | IntExpr   of {num:int}
  | SeqExpr   of {el:expr list}
  | NegExpr   of {e:expr}
  | InfixExpr of {e1:expr; op:infixOp; e2:expr}
  | WhileExpr of {e1:expr; e2:expr}
  | IfThenElseExpr  of {e1:expr; e2:expr; e3:expr}
  | IfThenExpr      of {e1:expr;e2:expr}
  | LetExpr   of {dl:dec list; el:expr list}
  | ForExpr   of {id:ident;init:expr;finish:expr;e:expr}
and infixOp = 
    Add 
  | Sub 
  | Mul 
  | Div 
  | Eq 
  | Neq 
  | Lt 
  | Gt 
  | Geq 
  | Leq 
  | And
  | Or 
  | Asgn 


(* AST pretty printer. 
 * Parent/child relationships are defined by indentation, e.g.:
 *   
 *   PARENT
 *     CHILD 1 
 *       CHILD 1.1 
 *     CHILD 2 
 * 
 * Means that CHILD 1 is the left child of PARENT, and CHILD 2 is the right 
 * child of parent, whereas CHILD1.1 is the child of CHILD 1. *)
let print_ast a =   
  let rec print_node str indent = 
    Printf.printf "%s\n" 
    (String.make indent ' ' ^ str)

  (* indent amount *)
  and x = 4 

  and print_dec d indent = 
    match d with 
      TyDec{tyId:ident;t:ty}  ->print_node ("TyDec:") indent; 
                              print_node ("tyId(" ^ tyId ^ ")") (indent+x);
                              print_ty t (indent+x); 
    | VarDec{id:ident;e:expr} -> print_node ("VarDec:") indent; 
                              print_node ("id(" ^ id ^ ")") (indent+x);
                              print_expr e (indent+x);
    | TyVarDec{id:ident;tyId:ident;e:expr}
                              -> print_node ("TyVarDec:") indent; 
                              print_node ("id(" ^ id ^ ")") (indent+x);
                              print_node ("tyId(" ^ tyId ^ ")") (indent+x);
                              print_expr e (indent+x);
    | FunDec{id:ident;fl:fieldDec list;e:expr}
                              -> print_node ("FunDec:") indent; 
                              print_node ("id(" ^ id ^ ")") (indent+x); 
                              print_fieldDecList fl (indent+x);     
                              print_expr e (indent+x);
    | TyFunDec{id:ident;fl:fieldDec list;tyId:ident;e:expr} 
                              -> print_node ("TyFunDec:") indent; 
                              print_node ("id(" ^ id ^ ")") (indent+x); 
                              print_fieldDecList fl (indent+x);     
                              print_node ("tyId(" ^ tyId ^ ")") (indent+x);
                              print_expr e (indent+x);

  and print_ty t indent = 
    match t with 
      TyId{tyId:ident}        ->print_node ("tyId(" ^ tyId ^ ")") (indent)
    | ArrTy{tyId:ident}       ->print_node ("ArrTy(" ^ tyId ^ ")") (indent)
    | RecTy{fl:fieldDec list} ->print_node ("RecTy") indent;
                              print_fieldDecList fl (indent+2);

  and print_fieldDec f indent = 
    match f with 
      FieldDec{id:ident;tyId:ident}   
                              ->print_node ("FieldDec:") indent; 
                              print_node ("id(" ^ id ^ ")") (indent+x); 
                              print_node ("tyId(" ^ tyId ^ ")") (indent+x)

  and print_fieldDecList fl indent = 
    match fl with 
      [] -> ()
    | hd::tl -> print_fieldDec hd indent; print_fieldDecList tl indent;

  and print_lVal lval indent = 
    match lval with 
      Id{id:ident}            -> print_node ("LVal: Id(" ^ id ^ ")") indent
    | Arr{lval:lVal;e:expr}   -> print_node ("LVal: Arr") indent; 
                              print_lVal lval (indent+x); 
                              print_expr e (indent+x);
    | FieldExpr{lval:lVal;id:ident}   
                              -> print_node ("LVal: FieldExpr") indent; 
                              print_lVal lval (indent+x); 
                              print_node ("Id(" ^ id ^ ")") (indent+x)
 
  and print_expr a indent =
    match a with 
    | LVal{lval:lVal}         -> print_lVal lval (indent);
    | NilExpr                 -> print_node "NilExpr: Nil" indent;
    | IntExpr{num:int}        -> print_node ("IntExpr:" ^ string_of_int num)
                                            indent
    | SeqExpr{el:expr list}   -> print_node "SeqExpr:" indent;
                              print_exprList el (indent+x);
    | NegExpr{e:expr}         -> print_node "NegExpr:" indent;
                              print_expr e (indent+x);
    | InfixExpr{e1:expr;op:infixOp;e2:expr}  
                              -> print_node "InfixExpr:" indent;
                              print_expr e1 (indent+x);
                              print_node (string_of_infixOp op) (indent+x);
                              print_expr e2 (indent+x)
    | WhileExpr{e1:expr;e2:expr} 
                              -> print_node "WhileExpr:" indent; 
                              print_expr e1 (indent+x); 
                              print_expr e2 (indent+x);
    | IfThenElseExpr{e1:expr;e2:expr;e3:expr} 
                              -> print_node "IfThenElseExpr:" indent; 
                              print_expr e1 (indent+x);
                              print_expr e2 (indent+x);
                              print_expr e3 (indent+x);
    | IfThenExpr{e1:expr;e2:expr} 
                              -> print_node "IfThenExpr:" indent; 
                              print_expr e1 (indent+x);
                              print_expr e2 (indent+x);
    
    | LetExpr{dl:dec list;el:expr list} 
                              -> print_node "LetExpr:" indent; 
                              print_decList dl (indent+x);
                              print_exprList el (indent+x);                             
  
  and print_decList dl indent = 
    match dl with 
      [] -> () 
    | hd::tl -> print_dec hd indent; print_decList tl indent; 

  and print_exprList el indent = 
    match el with 
      [] -> () 
    | hd::tl -> print_expr hd indent; print_exprList tl indent;

  and string_of_infixOp op =
    match op with 
      Add   -> "Add (+)" 
    | Sub   -> "Sub (-)" 
    | Mul   -> "Mul (*)"
    | Div   -> "Div (/)" 
    | Eq    -> "Eq (=)"
    | Neq   -> "Neq (<>)"
    | Lt    -> "Lt (<)"
    | Gt    -> "Gt (>)"
    | Geq   -> "Geq (>=)" 
    | Leq   -> "Leq (<=)"
    | And   -> "And (&)"
    | Or    -> "Or (|)"
    | Asgn  -> "Asgn (:=)" 
  
  in
  print_expr a 0
