%{
  open Ast
%}

%token EOF 
%token <int> INT 
%token <Ast.ident> ID
%token <string> STRING 
%token COMMA COLON SEMICOLON LPAREN RPAREN LBRACK RBRACK 
%token LBRACE RBRACE DOT 
%token PLUS MINUS TIMES DIVIDE EQ NEQ LT GT GEQ LEQ
%token AND OR ASSIGN 
%token ARRAY IF THEN ELSE WHILE FOR TO DO LET IN END OF
%token BREAK NIL 
%token FUNCTION VAR TYPE 

(* Precedence levels and association for each level. Low to high. *)
%left ASSIGN
%left OR 
%left AND 
%nonassoc EQ NEQ LT GT GEQ LEQ 
%left PLUS MINUS
%left TIMES DIVIDE 
%nonassoc THEN 
%nonassoc ELSE
%nonassoc DO

%start prog
%type <Ast.expr option> prog

%%

prog:
  | EOF { None } 
  | e = expr EOF { Some e }

dec:
  | TYPE ID EQ ty { TyDec{tyId=$2;t=$4} }
  | VAR ID ASSIGN expr { VarDec{id=$2;e=$4} }
  | VAR ID COLON ID ASSIGN expr { TyVarDec{id=$2;tyId=$4;e=$6} }
  | FUNCTION ID LPAREN fieldDecList RPAREN EQ expr 
      { FunDec{id=$2;fl=$4;e=$7} }
  | FUNCTION ID LPAREN RPAREN EQ expr 
      { FunDec{id=$2;fl=[];e=$6} }
  | FUNCTION ID LPAREN fieldDecList RPAREN COLON ID EQ expr
      { TyFunDec{id=$2;fl=$4;tyId=$7;e=$9} }
  | FUNCTION ID LPAREN RPAREN COLON ID EQ expr
      { TyFunDec{id=$2;fl=[];tyId=$6;e=$8} }
  
ty:
  | ID  { TyId{tyId=$1} }
  | ARRAY OF ID { ArrTy{tyId=$3}  }
  | LBRACE fieldDecList RBRACE { RecTy{fl=$2} } 
  | LBRACE RBRACE { RecTy{fl=[]} }

fieldDec:
  | ID COLON ID { FieldDec{id=$1;tyId=$3} }

fieldDecList:
  | fieldDec { [$1] } 
  | fieldDec COMMA fieldDecList { $1::$3 }

lVal:
  | ID { Id{id=$1} }
  | lVal LBRACK expr RBRACK  {Arr{lval=$1;e=$3} }
  | lVal DOT ID { FieldExpr{lval=$1;id=$3} }

(* expr. *)
expr:
  | lVal { LVal {lval=$1} }
  | NIL { NilExpr }
  | INT { IntExpr {num=$1}} 
  | seqExpr { SeqExpr {el=$1} }
  | MINUS expr { NegExpr {e=$2} }
  | expr PLUS expr { InfixExpr{e1=$1; op=Add; e2=$3} }
  | expr MINUS expr { InfixExpr{e1=$1; op=Sub; e2=$3} }
  | expr TIMES expr { InfixExpr{e1=$1; op=Mul; e2=$3} }
  | expr DIVIDE expr { InfixExpr{e1=$1; op=Div; e2=$3} }
  | expr EQ expr { InfixExpr{e1=$1; op=Eq; e2=$3} }
  | expr NEQ expr { InfixExpr{e1=$1; op=Neq; e2=$3} }
  | expr LT expr { InfixExpr{e1=$1; op=Lt; e2=$3} }
  | expr GT expr { InfixExpr{e1=$1; op=Gt; e2=$3} }
  | expr GEQ expr { InfixExpr{e1=$1; op=Geq; e2=$3} }
  | expr LEQ expr { InfixExpr{e1=$1; op=Leq; e2=$3} }
  | expr AND expr { InfixExpr{e1=$1; op=And; e2=$3} }
  | expr OR expr { InfixExpr{e1=$1; op=Or; e2=$3} }
  | expr ASSIGN expr { InfixExpr{e1=$1; op=Asgn; e2=$3} }
  | WHILE expr DO expr { WhileExpr{e1=$2; e2=$4} }
  | IF expr THEN expr ELSE expr { IfThenElseExpr{e1=$2; e2=$4; e3=$6} }
  | IF expr THEN expr { IfThenExpr{e1=$2;e2=$4} }
  | FOR ID ASSIGN expr TO expr DO expr 
      { ForExpr{id=$2;init=$4;finish=$6;e=$8} }
  | LET decList IN exprList END { LetExpr{dl=$2;el=$4} }
  | LET decList IN END { LetExpr{dl=$2;el=[]} }

(* expr helper productions. *)

decList:
  | dec { [$1] }
  | dec decList { $1::$2 }
seqExpr:
  | LPAREN RPAREN { [] } 
  | LPAREN exprList RPAREN { $2 } 

exprList:
  | expr  { [$1] } 
  | expr SEMICOLON  { [$1] } 
  | expr  SEMICOLON exprList { $1::$3 }

%%
